import Foundation
import shared

class Collector<T> : Kotlinx_coroutines_coreFlowCollector {
    let callback:(T) -> Void

    init(callback: @escaping (T) -> Void) {
        self.callback = callback
    }
    
    func emit(value: Any?) async throws -> KotlinUnit {
        if let v = value as? T {
            callback(v)
        }
        return KotlinUnit()
    }
}
